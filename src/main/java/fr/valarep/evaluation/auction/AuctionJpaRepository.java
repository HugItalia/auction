package fr.valarep.evaluation.auction;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuctionJpaRepository extends JpaRepository<Auction , Long> {

    Auction findByProduct(String product);
    void deleteById(Long id);
}
