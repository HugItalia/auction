package fr.valarep.evaluation.auction;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.Objects;

@Entity
public class Auction {

    @Id
    @GeneratedValue
    private Long id;
    private Date startDate;
    private Date endDate;
    private Double startPrice;
    private String product;

    public Auction() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Auction auction = (Auction) o;
        return Objects.equals(id, auction.id) &&
                Objects.equals(startDate, auction.startDate) &&
                Objects.equals(endDate, auction.endDate) &&
                Objects.equals(startPrice, auction.startPrice) &&
                Objects.equals(product, auction.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, startDate, endDate, startPrice, product);
    }

    @Override
    public String toString() {
        return "Auction{" +
                "id=" + id +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", startPrice=" + startPrice +
                ", product='" + product + '\'' +
                '}';
    }

    public Auction(Date startDate, Date endDate, Double startPrice, String product) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startPrice = startPrice;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(Double startPrice) {
        this.startPrice = startPrice;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}
