package fr.valarep.evaluation.auction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/auctions")
public class AuctionController {

    @Autowired
    AuctionJpaRepository auctionRepository;

    @GetMapping("auctions")
    public List<Auction> all() {
        return auctionRepository.findAll();
    }

    @GetMapping("auction/{name}")
    public Auction oneAuction(@PathVariable String product) {
        return auctionRepository.findByProduct(product);
    }

    @PostMapping("auctions")
    public void save(@RequestBody Auction auction) {
        auctionRepository.save(auction);
    }

    @PutMapping("auctions")
    public void update(@RequestBody Auction auction) {
        auctionRepository.save(auction);
    }

    @DeleteMapping("auctions")
    public void deleteAll() {auctionRepository.deleteAll();}

    @DeleteMapping("auctions/{id}")
    public void delete(@PathVariable long id) {
        auctionRepository.deleteById(id);
    }


}
