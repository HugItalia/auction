package fr.valarep.evaluation.auction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AuctionRepository {

    private static final String INSERT_QUERY = "insert into auction(startDate, endDate, startPrice, product)"
            + "values (:startDate, :endDate, :startPrice, :product);";


    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public AuctionRepository(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Auction> findAll() {
        return namedParameterJdbcTemplate.query("select * from auction", new AuctionMapper());
    }

    public void save(Auction auction) {
        String sql = "insert into auction(startDate, endDate, startPrice, product)" + "values (:startDate, :endDate, :startPrice, :product);";

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("startDate", auction.getStartDate());
        parameters.put("endDate", auction.getEndDate());
        parameters.put("startPrice", auction.getStartPrice());
        parameters.put("product", auction.getProduct());

        namedParameterJdbcTemplate.update(sql, parameters);
    }

    private static final class AuctionMapper implements RowMapper<Auction> {

        public Auction mapRow(ResultSet rs, int rowNum) throws SQLException {
            Auction auction = new Auction();
            auction.setId(rs.getLong("id"));
            auction.setStartDate(rs.getDate("startDate"));
            auction.setEndDate(rs.getDate("endDate"));
            auction.setStartPrice(rs.getDouble("startPrice"));
            auction.setProduct(rs.getString("product"));
            return auction;
        }

    }

}
